# Station

This repository houses scripts and documents pertaining to the Pikes Peak Radio Amateurs Association club station, especially checklists, equipment manuals, and cheat sheets.

Housing these documents in a source code repository allows club members to discuss them in the issue tracker, and to make use of collaborative tools like Wikis to develop them further.

**Are you just looking to print out additional copies of these checklists?** The following files are what you need:

* [station-checklist-titlepage.pdf](https://codeberg.org/PPRAA/Station/src/branch/main/station-checklist-titlepage.pdf) is the front page of the binder.
* [station-checklist-1-operations.pdf](https://codeberg.org/PPRAA/Station/src/branch/main/station-checklist-1-operations.pdf) is the checklist for normal operations: what to do to check in/check out of the station.
* [station-checklist-2-maintenance.pdf](https://codeberg.org/PPRAA/Station/src/branch/main/station-checklist-2-maintenance.pdf) is the checklist of station maintenance items to be performed once per quarter, to keep the station up and running smoothly.

The .fodt files are the source code for these documents; you may submit pull requests if these need to be updated with new checklist items.

## Projects and issues

Is something broken at the station? Do you have ideas for improvements and new features? Use the issue tracker at [https://codeberg.org/PPRAA/Station/issues](https://codeberg.org/PPRAA/Station/issues).